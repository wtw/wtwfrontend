// import logo from "../Images/Logo.png";
import blog from "../Images/Blog.svg";
import recipe from "../Images/Recipe.svg";
import "../css/Tile.css";

// import { Card, CardGroup } from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Button, CardGroup, CardDeck } from "react-bootstrap";
// import Button from "@bit/react-bootstrap.react-bootstrap.button";
// import Card from "@bit/react-bootstrap.react-bootstrap.card";

const ButtonStyle = {
  width: "50%",
  marginLeft: "25%",
  marginBottom: "5%",
  backgroundColor: "#d1b22b",
  color: "#212529",
  fontWeight: "bold",
};

const CardStyle = {
  margin: "1.5rem",
  border: "2px solid #d1b22b",
};

export default function Tile(props) {
  return (
    // <div className="card1" style={props.stylecss}>
    //   <img className="card-img-top" src={props.logo} alt="Card image cap"></img>
    //   <div className="card-body" style={props.cbodycss}>
    //     <h5 className="card-title">{props.cardTitle}</h5>
    //     <p className="card-text">{props.cardText}</p>

    //     <a href={props.url} className="btn btn-primary">
    //       Select
    //     </a>
    //   </div>
    // </div>
    <div>
      <br></br>
      <div class="card m-5 border-0 shadow" rel="stylesheet">
        <div class="container">
          <h3>Welcome, Sarah Millington</h3>
          <p>Calories intake per day: </p>
          <p>Time left till you reach your goal:</p>
        </div>
      </div>

      <br></br>

      <CardDeck className="m-5 border-0 shadow">
        <Card style={CardStyle}>
          <Card.Img variant="top" src={props.logo} />
          <Card.Body>
            <Card.Title style={{ textAlign: "center" }}>Workout</Card.Title>
            <Card.Text style={{ textAlign: "center" }}>
              View workouts for targeted area
            </Card.Text>
          </Card.Body>
          <a href={props.url} className="btn" style={ButtonStyle}>
            Workouts
          </a>
        </Card>
        <Card style={CardStyle}>
          <Card.Img variant="top" src={props.logo2} />
          <Card.Body>
            <Card.Title style={{ textAlign: "center" }}>Recipe </Card.Title>
            <Card.Text style={{ textAlign: "center" }}>
              Prep your next meal here.
            </Card.Text>
          </Card.Body>
          <a href={props.url} className="btn" style={ButtonStyle}>
            Recipes
          </a>
        </Card>
        <Card style={CardStyle}>
          <Card.Img variant="top" src={props.logo3} />
          <Card.Body>
            <Card.Title style={{ textAlign: "center" }}>Blog</Card.Title>
            <Card.Text style={{ textAlign: "center" }}>
              Connect with other user's.
            </Card.Text>
          </Card.Body>
          <a href={props.url} className="btn" style={ButtonStyle}>
            Blog
          </a>
        </Card>
      </CardDeck>
    </div>
  );
}
