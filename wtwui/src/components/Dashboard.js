import React, { Fragment } from "react";

import logo from "../Images/Logo.svg";
import workout from "../Images/Workout.svg";
import blog from "../Images/Blog.svg";
import recipe from "../Images/Recipe.svg";

import Tile from "./Tile";

export default function Dashboard() {
  return (
    <Fragment>
      <Tile
        logo={workout}
        logo2={recipe}
        logo3={blog}
        cardTitle={"Workouts"}
        cardText={"View workouts for targeted areas"}
        stylecss={{
          width: 18 + "rem",
          height: 30 + "rem",
        }}
      />
      {/* <Tile
        logo={logo}
        cardTitle={"Recipes"}
        stylecss={{
          width: 18 + "rem",
          height: 30 + "rem",
        }}
      />
      <Tile
        logo={logo}
        cardTitle={"Blog"}
        stylecss={{
          width: 18 + "rem",
          height: 30 + "rem",
        }}
      /> */}
    </Fragment>
  );
}
