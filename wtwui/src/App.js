import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import { Provider } from 'react-redux';
import store from './store';
import { loadUser } from './actions/auth';

import "./App.css";
import "./css/common.css";
import Navbar from "./components/Navbar";
import "bootstrap/dist/css/bootstrap.css";
import Dashboard from "./components/Dashboard";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import PrivateRoute from "./components/common/PrivateRoute";
import Alerts from "./components/layout/Alerts";
import Login from "./components/Login";
import SignUp from "./components/Signup.";
import logo from "./Images/Logo.svg";

function Header() {
  return <img src={logo.svg} alt="Logo" />;
}

const alertOptions = {
  timeout: 3000,
  position: 'top center',
};

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }
  render(){
    return(
      <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...alertOptions}>
          <Router>
            <Fragment>
              <Navbar/>
              <Alerts/>
              <div className="App">
                <Switch>
                  <PrivateRoute exact path="/" component={Dashboard} />
                  <Route exact path="/register" component={SignUp} />
                  <Route exact path="/login" component={Login} />
                </Switch>
              </div>
            </Fragment>
          </Router>
        </AlertProvider>
      </Provider>
    )
  }
}

export default App;
