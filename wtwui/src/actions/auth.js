import axios from "axios";
import { USER_LOADED, USER_LOADING, AUTH_ERROR, LOGOUT_SUCCESS, LOGIN_SUCCESS, LOGIN_FAIL } from "./types"
import { returnErrors } from './messages';

// CHECK TOKEN & LOAD USER


export const loadUser = () => (dispatch, getState) => {
    // User Loading
    dispatch({ type: USER_LOADING });
    axios.all([
        axios.get('http://localhost:8000/api/auth/user', tokenConfig(getState)),
        axios.get(`http://localhost:8002/fitnessapi/profile/5/get_profile/`, tokenConfig(getState))
    ])
    .then((res) => {
        console.log(res)
        dispatch({
            type: USER_LOADED,
            payload: res.data,
        });
    })
    .catch((err) => {
        dispatch(returnErrors(err.response.data, err.response.status));
        dispatch({
            type: AUTH_ERROR,
        });
    });
};

// LOGIN USER
export const login = (username, password) => (dispatch) => {
    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    // Request Body
    const body = JSON.stringify({ username, password });

    axios
        .post('http://localhost:8000/api/auth/login', body, config)
        .then((res) => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: res.data,
            });
        })
        .catch((err) => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                type: LOGIN_FAIL,
            });
        });
};

// LOGOUT USER
export const logout = () => (dispatch, getState) => {
    axios
        .post('http://localhost:8000/api/auth/logout/', null, tokenConfig(getState))
        .then((res) => {
            dispatch({
                type: LOGOUT_SUCCESS,
            });
        })
        .catch((err) => {
            dispatch(returnErrors(err.response.data, err.response.status));
        });
};

export const tokenConfig = (getState) => {
    // Get token from state
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    // If token, add to headers config
    if (token) {
        config.headers['Authorization'] = `Token ${token}`;
    }

    return config;
};